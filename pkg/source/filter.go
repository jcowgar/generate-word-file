package source

import "strings"

func (file *File) Filter(keepCharacters string) []string {
	keepWords := []string{}
	excludeLetters := generateExclusionString(keepCharacters)

	for _, word := range file.Words {
		if !strings.ContainsAny(word, excludeLetters) {
			keepWords = append(keepWords, word)
		}
	}
	return keepWords
}

func generateExclusionString(keep string) string {
	alphabet := []string{
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
		"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
	}

	keepChars := strings.Split(keep, "")
	excludeChars := []string{}

	for _, a := range alphabet {
		found := false

		for _, b := range keepChars {
			if a == b {
				found = true

				break
			}
		}

		if !found {
			excludeChars = append(excludeChars, a)
		}
	}

	return strings.Join(excludeChars, "")
}
