package source

import "strings"

type ScoredWord struct {
	Word  string
	Score int
}

func NewScoredWord(word string, chars string) ScoredWord {
	return ScoredWord{
		Word:  word,
		Score: score(word, chars),
	}
}

func score(word string, chars string) int {
	count := 0

	for _, ch := range strings.Split(chars, "") {
		count += strings.Count(word, ch)
	}

	return count
}
