package source

import (
	"os"
	"strings"
)

type File struct {
	Filename string

	Words []string
}

func NewFile(filename string) (*File, error) {
	file := &File{
		Filename: filename,
	}

	raw, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	for _, word := range strings.Split(string(raw), "\n") {
		word := word
		file.Words = append(file.Words, strings.TrimSpace(word))
	}

	return file, nil
}
