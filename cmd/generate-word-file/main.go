package main

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"path"
	"sort"
	"strings"
	"time"

	"codeberg.org/jcowgar/generate-word-file/pkg/source"
	"github.com/urfave/cli/v2"
)

var (
	outputDirectory string
	minChars        int
	maxChars        int
	maxWords        int
	focusChars      string
)

func main() {
	app := &cli.App{
		Name:            "generate-word-file",
		Usage:           "generate a word subset from a source file",
		Version:         "0.1.0",
		HideHelpCommand: true,
		Compiled:        time.Now(),
		Action:          run,
		ArgsUsage:       "SOURCE-FILE char-list [char-list...]",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "output-directory",
				Aliases:     []string{"o"},
				Usage:       "write files to `DIR`",
				Destination: &outputDirectory,
			},
			&cli.IntFlag{
				Name:        "min-chars",
				Usage:       "minimum word size of `COUNT`",
				Destination: &minChars,
				DefaultText: "1",
			},
			&cli.IntFlag{
				Name:        "max-chars",
				Usage:       "maximum word size of `COUNT`",
				Destination: &maxChars,
				DefaultText: "30",
			},
			&cli.IntFlag{
				Name:        "max-words",
				Usage:       "include a maximum of `COUNT` words",
				Destination: &maxWords,
				DefaultText: "5000",
			},
			&cli.StringFlag{
				Name:        "focus-chars",
				Usage:       "focus on `CHARS`",
				Destination: &focusChars,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		os.Exit(1)
	}
}

func run(ctx *cli.Context) error {
	file, err := source.NewFile(ctx.Args().Get(0))
	if err != nil {
		return err
	}

	if maxChars == 0 {
		maxChars = 30
	}

	if maxWords == 0 {
		maxWords = 5000
	}

	if outputDirectory == "" {
		outputDirectory = "."
	}

	var (
		words          []string
		outputFilename string
	)

	if focusChars != "" {
		outputFilename = path.Join(outputDirectory, fmt.Sprintf("focus-%s.txt", focusChars))
		words = file.Words
	} else {
		keepLetters := ctx.Args().Get(1)
		outputFilename = path.Join(outputDirectory, fmt.Sprintf("%s.txt", keepLetters))
		words = file.Filter(keepLetters)
	}

	newKeepWords := []string{}

	for _, word := range words {
		focus := focusChars == "" || strings.ContainsAny(word, focusChars)

		if focus && len(word) >= minChars && len(word) <= maxChars {
			newKeepWords = append(newKeepWords, word)
		}
	}

	if focusChars != "" {
		scoredWords := []source.ScoredWord{}
		for _, w := range newKeepWords {
			scoredWords = append(scoredWords, source.NewScoredWord(w, focusChars))
		}

		sort.Slice(scoredWords, func(i, j int) bool {
			return scoredWords[i].Score < scoredWords[j].Score
		})

		newKeepWords = []string{}

		cnt := 0
		for i := len(scoredWords) - 1; i >= 0; i-- {
			newKeepWords = append(newKeepWords, scoredWords[i].Word)

			cnt++

			if cnt == maxWords {
				break
			}
		}
	}

	words = maxElements(maxWords, newKeepWords)

	output := strings.Join(words, " ")

	return os.WriteFile(outputFilename, []byte(output), 0o644)
}

func maxElements(max int, ary []string) []string {
	rand.Seed(time.Now().UnixMicro())

	min := int(math.Min(float64(max), float64(len(ary))))

	rand.Shuffle(len(ary), func(i, j int) {
		ary[i], ary[j] = ary[j], ary[i]
	})

	return ary[0:min]
}
