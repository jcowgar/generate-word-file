Generate Word File
==================

Generate Word File will take a master word list file and a list of
letters. From that it will generate a space delimited list of words
containing only those letters.

This can be used for many purposes but my original reason for developing the
program was to create progressing word files to help me learn the CharaChorder
Keyboard (https://www.charachorder.com/).

See the `Makefile` file for example use.

The English word file came from https://github.com/dwyl/english-words/