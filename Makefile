.PHONY: clean

all: generate-word-file erta.txt ertaon.txt ertaonil.txt ertaonilju.txt \
	ertaoniljusy.txt ertaoniljusycd.txt ertaoniljusycdkf.txt \
	ertaoniljusycdkfmh.txt ertaoniljusycdkfmhvp.txt ertaoniljusycdkfmhvpzq.txt \
	ertaoniljusycdkfmhvpzqwb.txt ertaoniljusycdkfmhvpzqwbxg.txt

DEPS := $(wildcard ./cmd/generate-word-file/*.go) $(wildcard pkg/source/*.go)

generate-word-file: $(DEPS)
	go build -o $@ ./cmd/generate-word-file

erta.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaon.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaonil.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaonilju.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusy.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycd.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycdkf.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycdkfmh.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycdkfmhvp.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycdkfmhvpzq.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycdkfmhvpzqwb.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

ertaoniljusycdkfmhvpzqwbxg.txt: generate-word-file support/words_alpha.txt
	./generate-word-file --max-words 5000 --min-chars 3 --max-chars 8 support/words_alpha.txt $(subst .txt,,$@)

clean:
	rm generate-word-file erta*.txt